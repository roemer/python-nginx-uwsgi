from flask import Flask, escape, request

app = Flask(__name__)


@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def catch_all(path):
    name = request.args.get("name", "World")
    return f"Hello, {escape(name)} from '{path}'!"