#!/usr/bin/env bash

set -x #echo on

envsubst '$PORT' < /etc/nginx/sites-available/default.template > /etc/nginx/sites-enabled/default
retVal=$?
if [ $retVal -ne 0 ]; then
    echo "ERROR: envsubst returns $retVal"
    exit $retVal
fi

# additional configuration
echo "Calling /etc/before-start.sh"
bash /etc/before-start.sh
retVal=$?
if [ $retVal -ne 0 ]; then
    echo "ERROR: /etc/before-start.sh returns $retVal"
    exit $retVal
fi
exec supervisord --nodaemon -c /etc/supervisor/supervisord.conf