IMAGE_NAME = python-stack

docker: # dist
	docker build -t $(IMAGE_NAME) . --no-cache

docker-run:
	docker run -p 80:80 $(IMAGE_NAME)
