ARG BASE_IMAGE=python:3.6-slim-buster

FROM $BASE_IMAGE

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        nginx supervisor curl gettext-base \
        # build tools: needed for uwsgi
        g++ python-dev libpcre3-dev && \
    # install python packages
    pip install --no-cache-dir uwsgi pipenv flask && \
    # remove build tools needed for uWSGI, clean cache
    apt-get remove -y --purge g++ python-dev libpcre3-dev && \
    # clear up apt
    apt-get autoremove --purge -y && apt-get clean && rm -rf /var/lib/apt/lists/*

# copy the file system
COPY ./root/ /

# Set owner of /app to www-data
RUN chown -hR www-data:www-data /app

ENV FLASK_APP=/app/app.py PYTHONPATH=/app/ APP_DIR=/app/ APP_MODULE=app APP_CALLABLE=app PORT=80 UWSGI_WORKERS=5

EXPOSE 80

CMD ["/bin/bash", "/etc/entrypoint.sh"]
