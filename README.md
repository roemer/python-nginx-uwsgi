# Python 3.6/3.7/3.8/3.9 + Supervisord + nginx + uWSGI image  

![build status](https://gitlab.com/roemer/python-nginx-uwsgi/badges/master/pipeline.svg)

This is a standard Docker image for hosting Python apps using Supervisord, nginx and uWSGI. It contains a _very_ basic Flask example. 

The basis for this image are the standard `python:3.x-slim` images, itself based on Debian 10 'Buster'. 

The following images are available:

| Tags | Id | Description |
| ---- | -- | ----------- |
| `3.6` | `registry.gitlab.com/roemer/python-nginx-uwsgi:3.6` | The latest [Python 3.6 image](https://hub.docker.com/_/python) |
| `3.7` | `registry.gitlab.com/roemer/python-nginx-uwsgi:3.7` | The latest [Python 3.7 image](https://hub.docker.com/_/python) |
| `3.8` | `registry.gitlab.com/roemer/python-nginx-uwsgi:3.8` | The latest [Python 3.8 image](https://hub.docker.com/_/python) |
| `3.9` | `registry.gitlab.com/roemer/python-nginx-uwsgi:3.9` | The latest [Python 3.9 image](https://hub.docker.com/_/python) |
| `3.6-odbc` | `registry.gitlab.com/roemer/python-nginx-uwsgi:3.6-odbc` |  The latest [Python 3.6 image](https://hub.docker.com/_/python), extended with the [SQL Server ODBC driver](https://docs.microsoft.com/en-us/sql/connect/odbc/linux-mac/installing-the-microsoft-odbc-driver-for-sql-server?view=sql-server-ver15) |
| `3.7-odbc` | `registry.gitlab.com/roemer/python-nginx-uwsgi:3.7-odbc` |  The latest [Python 3.7 image](https://hub.docker.com/_/python), extended with the [SQL Server ODBC driver](https://docs.microsoft.com/en-us/sql/connect/odbc/linux-mac/installing-the-microsoft-odbc-driver-for-sql-server?view=sql-server-ver15) |
| `3.8-odbc` | `registry.gitlab.com/roemer/python-nginx-uwsgi:3.8-odbc` |  The latest [Python 3.8 image](https://hub.docker.com/_/python), extended with the [SQL Server ODBC driver](https://docs.microsoft.com/en-us/sql/connect/odbc/linux-mac/installing-the-microsoft-odbc-driver-for-sql-server?view=sql-server-ver15) |
| `3.9-odbc` | `registry.gitlab.com/roemer/python-nginx-uwsgi:3.9-odbc` |  The latest [Python 3.9 image](https://hub.docker.com/_/python), extended with the [SQL Server ODBC driver](https://docs.microsoft.com/en-us/sql/connect/odbc/linux-mac/installing-the-microsoft-odbc-driver-for-sql-server?view=sql-server-ver15) |

The images can be found on `registry.gitlab.com/roemer/python-nginx-uwsgi`; see https://gitlab.com/roemer/python-nginx-uwsgi/container_registry

The base image size is around 100 MiB. 

## Getting started

1. Create a `Dockerfile` with this image as a starting point:
    
    ```dockerfile
    FROM registry.gitlab.com/roemer/python-nginx-uwsgi:3.6
    
    # Copy your application code in
    COPY ./my-app/ /app/
   
    # Install Python dependencies 
    WORKDIR /app
    RUN pipenv install --ignore-pipfile --system
    ``` 

2. Build the Docker:

    ```bash
    docker build . -t my-image-name:latest 
    ```
   
3. Test it locally:
   
   Start the Docker first:
    ```bash
    docker run -p 80:80 my-image-name:latest
    ```
   
   Now fire a request, and see if all is well:
   
   `curl -v "http://localhost/"` 
   
   The default app returns a cheery "Hello, World!"

## Configuration

The configuration of Supervisord, nginx, and uWSGI can be extended or overwritten by putting files in the following structure: 

```text
    / (root)
    ├── app                             => website folder
    │   ├── app.py                      => Flask module
    │   └── ui                          => static files folder
    │       ├── index.html
    │       └── static              
    │           └── logo.png
    └── etc
        ├── before-start.sh             => Bash file for additional startup logic
        ├── entrypoint.sh               => Standard Docker entrypoint; calls before-start.sh, 
        |                                   then starts supervisord.
        ├── nginx
        │   ├── nginx.conf              => Standard nginx configuration
        │   ├── sites-available
        │   │   └── default.template    => site-specific configuration; this template is used during startup 
        |   |                               to generate /etc/nginx/sites-enabled/default.           
        │   └── sites-enabled
        ├── supervisor
        │   ├── conf.d
        │   │   ├── nginx.conf          => nginx startup configuration
        │   │   ├── uwsgi.conf          => uWSGI startup configuration
        |   |   └── *.conf              => other Supervisor process configuration files (add if needed) 
        │   └── supervisord.conf        => Standard supervisord configuration
        └── uwsgi
            └── uwsgi.ini               => uWSGI configuration
```

See the [`root`](https://gitlab.com/roemer/python-nginx-uwsgi/tree/master/root) directory.   

## uWSGI 

The Python uWSGI app (e.g. Flask) should be placed in `/app/app.py`, and have a callable named `app`. In `/etc/uwsgi/uwsgi.ini`:
 
```ini
# Defaults: APP_DIR=/app/, APP_MODULE=app, APP_CALLABLE=app
chdir = $(APP_DIR)
module = $(APP_MODULE)
callable = $(APP_CALLABLE)
``` 

Further configuration can be done by setting environment variables `APP_DIR`, `APP_MODULE`, and `APP_CALLABLE`.

The base image contains the following Flask example code in the file `/app/app.py`:

```python
from flask import Flask, escape, request

app = Flask(__name__)

@app.route('/')
def hello():
    name = request.args.get("name", "World")
    return f'Hello, {escape(name)}!'
```

## Supervisor
Supervisor is configured to start nginx and uWSGI. More processes can be started by placing additional `.conf` files in `/etc/supervisor/conf.d`.

This is the startup configuration for uWSGI:

```ini
[program:uwsgi]
command=uwsgi --ini /etc/uwsgi/uwsgi.ini --die-on-term

stdout_logfile=/dev/stdout
stdout_logfile_maxbytes=0

stderr_logfile=/dev/stdout
stderr_logfile_maxbytes=0

stopsignail = QUIT
startsecs = 0
autorestart = false
autostart=true
``` 
 